extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var current_scene = get_tree().get_current_scene().get_name()
	if current_scene == "StageOne":
		if global.start == false:
			global.local_time = global.time
			global.start = true
			global.speed = 50
			global.max_speed = 100
			global.lives = 3
			global.alreadyDead = false

		if global.alreadyDead == true:
			global.local_time = global.time
			global.start = true
			global.speed = 50
			global.max_speed = 100
			global.lives = 3
			global.alreadyDead = false

	$Timer.start()



func _on_Timer_timeout():
	global.local_time -= 1
	if global.local_time == 0:
		$Timer.stop()
		get_tree().change_scene(str("res://Scenes/GameOver.tscn"))


