extends KinematicBody2D

export (int) var gravity = 1200
export (int) var jump_speed = -500
const up = Vector2(0, -1)
var velocity = Vector2()
var isAttacking = false
var isDead = false
var isMoving = false
onready var sprite = get_node("AnimatedSprite")
onready var particles = get_node("Particles2D")


func get_input():
	if isDead == false:
		if global.hasDead == true:
			particles.emitting = true
			if global.lives == 2:
				particles.lifetime = 3
			elif global.lives == 1:
				particles.lifetime = 0.5
			else:
				pass
		elif global.hasDead == false:
			particles.emitting = false

		var friction = false
		if Input.is_action_pressed("ui_right") && isAttacking == false:
			isMoving = true
			sprite.flip_h = false
			velocity.x = min(velocity.x + global.speed, global.max_speed)
			$AttackArea.rotation_degrees = 0
			$AnimatedSprite.play("walk")
		elif Input.is_action_pressed("ui_left") && isAttacking == false:
			isMoving = true
			sprite.flip_h = true
			$AttackArea.rotation_degrees = 180
			velocity.x = max(velocity.x - global.speed, -global.max_speed)
			$AnimatedSprite.play("walk")
		else:
			isMoving = false
			friction = true
			if isAttacking == false:
				$AnimatedSprite.play("idle")
		if Input.is_action_just_pressed("attack"):
			$AnimatedSprite.play("slash")
			isAttacking = true
			$AttackArea/CollisionShape2D.disabled = false
		if is_on_floor():
			if Input.is_action_pressed("ui_up") && isAttacking == false:
				velocity.y = jump_speed
			if friction == true:
				velocity.x = lerp(velocity.x, 0, 0.2)
		else:
			if velocity.y < 0:
				$AnimatedSprite.play("jump")
			if velocity.y > 0:
				$AnimatedSprite.play("fall")
			if friction == true:
				velocity.x = lerp(velocity.x, 0, 0.2)


func dead():
	if global.alreadyDead == true:
		pass
	else:
		print("mati")
		global.hasDead = true
		global.lives -= 1
		global.speed += 50
		global.max_speed += 50
		if global.lives <= 0:
			isDead = true
			global.start = false
			global.hasDead = false
			velocity = Vector2(0, 0)
			$AnimatedSprite.play("dead")
			global.alreadyDead = true
			$DeadTimer.start(2)

		else:
			var current_scene = get_tree().get_current_scene().get_name()
			get_tree().change_scene(str("res://Scenes/" + current_scene + ".tscn"))


func _physics_process(delta):
	velocity.y += delta * gravity
	get_input()
	velocity = move_and_slide(velocity, up)


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "slash":
		$AttackArea/CollisionShape2D.disabled = true
		isAttacking = false


func _on_AttackArea_body_entered(body):
	if "EnemyFrog" in body.name:
		body.dead()


func _on_DeadTimer_timeout():
	get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
