extends KinematicBody2D

export (int) var gravity = 1200
export (int) var speed = 50

const up = Vector2(0,-1)

var velocity = Vector2()

var direction = 1

var is_dead = false

func dead():
	is_dead = true
	velocity = Vector2(0,0)
	$CollisionShape2D.disabled = true
	$AnimatedSprite.play("dead")
	$Timer.start()
	
	
	
func _physics_process(delta):
	if is_dead==false:
		velocity.x = speed *direction
	
		if direction == 1:
			$AnimatedSprite.flip_h = true
		else:
			$AnimatedSprite.flip_h = false
		$AnimatedSprite.play("walk")
		velocity.y += delta * gravity;
	#get_input();
		velocity = move_and_slide(velocity, up);
	
	if is_on_wall():
		direction = direction * -1
		$RayCast2D.position.x *= -1
	if $RayCast2D.is_colliding() == false:
		direction = direction * -1
		$RayCast2D.position.x *= -1
	if get_slide_count() > 0:
		for i in range(get_slide_count()):
			if "Player" in get_slide_collision(i).collider.name:
				get_slide_collision(i).collider.dead()
		
	


func _on_Timer_timeout():
	queue_free()
