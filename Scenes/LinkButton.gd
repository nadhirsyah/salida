extends LinkButton

export(String) var scene_to_load	


func _on_LinkButton_pressed():
	get_tree().change_scene(str("res://Scenes/Tutorial.tscn"))


func _on_LinkButton2_pressed():
	get_tree().quit()


func _on_MainMenuButton_pressed():
	get_tree().change_scene(str("res://Scenes/TitleScreen.tscn"))


func _on_PlayButton_pressed():
	get_tree().change_scene(str("res://Scenes/StageOne.tscn"))
