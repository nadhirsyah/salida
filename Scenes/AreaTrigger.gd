extends Area2D

export (String) var sceneName = "StageOne"


func _on_AreaTrigger_body_entered(body):
	var current_scene = get_tree().get_current_scene().get_name()
	if body.get_name() == "Player":
		print("mati")
		global.lives -= 1
		global.speed += 50
		global.max_speed += 50
		global.hasDead = true
		if global.lives == 0:
			get_tree().change_scene(str("res://Scenes/GameOver.tscn"))
			global.lives = 3
			global.start = false
			global.alreadyDead = false
			global.hasDead = false
		else:
			get_tree().change_scene(str("res://Scenes/" + current_scene + ".tscn"))
